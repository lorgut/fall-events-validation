# Fall events validation

Repository which contains:

- The FAW acceleration fall analysis, ODS format files (FAWPerson{1,2}DataAnalysis.ods).
- The videos in which some of the analysed FAW falls can be watched. The videos have been cut from the original ones because the repository does not allow more than 25M.
- Different trials to define the testing rules for FAW falls, and some of the generated FAW falls using IoT-TEG 4.0 (TestingRulesforFAWEvents.ods)
- Comparison between original FAW falls and the IoT-TEG 4.0 generated ones (FAW falls comparison).
- A Java program with the EPL query, included in the ongoing prototype, which detects falls.
- Two extracted original falls for testing (JSON format) and the IoT-TEG 4.0 generated ones (JSON format). They are included in TestData folder.

To use the Java program type in a console: java -jar FallDetection.jar TestData/Fall1.json
